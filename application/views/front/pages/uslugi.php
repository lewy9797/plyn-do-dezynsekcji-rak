<section class="slider">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <div class="slider_image slider_image-subpage lazy" data-bg="url(<?php echo base_url(); ?>uploads/<?php echo $uslugi->photo; ?>)" alt="<?php echo $uslugi->alt; ?>">
            <h1 class="slider_title mb-3 wow fadeInUp"><?php echo $uslugi->title; ?></h1>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="container second_section">
    <div class="row">
      <?php $i=0; foreach ($rows as $v):
      if($i==3){$i=0;}
      $delay = 0.3 * $i; ?>
      <?php if($v->active == 1): ?>
        <a href="<?=  create_link('oferta', $v->title, $v->id) ?>" class="col-lg-4 col-md-6">
      <div class=" blog wow fadeIn" data-wow-delay="<?php echo $delay; ?>s">
        <picture class="">
          <source data-srcset="<?php echo base_url(); ?>uploads/<?php echo $v->photo; ?>.webp" type="image/webp"  class="img-fluid lazy mb-3 w-100">
          <source data-srcset="<?php echo base_url(); ?>uploads/<?php echo $v->photo; ?>" type="image/jpeg"  class="img-fluid mb-3 w-100 lazy"> 
          <img data-src="<?php echo base_url(); ?>uploads/<?php echo $v->photo; ?>" alt="<?php echo $v->alt; ?>"  class="lazy img-fluid mb-3 d-flex justify-content-center" style="max-height: 200px; margin: auto">
        </picture>
        <h3><?php echo $v->title; ?></h3>
        <div class="text-muted font-regular mt-4"><?php echo $v->short_desc; ?></div>
      </div>
    </a>
    <?php endif; ?>
      <?php $i++; endforeach ?>
    </div>
  </section>